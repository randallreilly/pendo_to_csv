import pandas as pd
import json
from urllib3 import request, response, poolmanager


class Pendo:

    def __init__(self):
        self.http = poolmanager.PoolManager()
        self.i_email = False
        self.i_email_value = ""
        self.i_date = False
        self.i_date_value = 0
        self.i_module = False
        self.i_module_value = ""
        self.i_tool = False
        self.i_tool_value = ""
        self.i_response = False

    def append_item(self, itr):
        self.i_response = True
        self.pendo_df.iat[itr-1, 0] = "'"+self.i_email_value+"'"
        self.pendo_df.iat[itr-1, 1] = self.i_date_value
        self.pendo_df.iat[itr-1, 2] = self.i_module_value
        self.pendo_df.iat[itr-1, 3] = self.i_tool_value

    def unravel_list(self, value):
        if self.i_tool and self.i_module and self.i_date and self.i_email:
            print("returning 0")
        for key in value:
            if type(key) is dict:
                self.unravel_dict(key)
            elif type(key) is list:
                self.unravel_list(key)

    def unravel_dict(self, value):
        for key in value:
            if self.i_tool and self.i_module and self.i_date and self.i_email:
                return
            temp_struct = value[key]
            if type(temp_struct) is dict:
                if key == 'app':
                    if not self.i_module:
                        self.i_module_value = value[key]
                        self.i_module = True
                if key == 'timeSeries':
                    if not self.i_module:
                        self.i_module_value = value[key]
                        self.i_module = True
                if key == 'generator':
                    if not self.i_tool:
                        self.i_tool_value = value[key]
                        self.i_tool = True
                if key == 'measure':
                    if not self.i_tool:
                        self.i_tool_value = value[key]
                        self.i_tool = True
                self.unravel_dict(value[key])
            elif type(temp_struct) is list:
                self.unravel_list(value[key])
            else:
                if key =='username':
                    if not self.i_email:
                        self.i_email_value = value[key]
                        self.i_email = True
                if key == 'createdAt':
                    if not self.i_date:
                        self.i_date_value = value[key]
                        self.i_date = True
                if key == 'deletedAt':
                    if not self.i_date:
                        self.i_date_value = value[key]
                        self.i_date = True
                if key == 'lastLogin':
                    if not self.i_date:
                        self.i_date_value = value[key]
                        self.i_date = True
                if key == 'columnLabel':
                    if not self.i_module:
                        self.i_module_value = "'"+value[key]+"'"
                        self.i_module = True
                if key == 'functionName':
                    if not self.i_tool:
                        self.i_tool_value = "'"+value[key]+"'"
                        self.i_tool = True
                if key == 'identified':
                    if not self.i_tool:
                        self.i_tool_value = "'"+value[key]+"'"
                        self.i_tool = True
                if key == 'attribute':
                    if not self.i_tool:
                        self.i_tool_value = "'"+value[key]+"'"
                        self.i_tool = True
            if self.i_tool and self.i_module and self.i_date and self.i_email:
                return

    def parse_resp(self, text, itr):
        self.i_email = False
        self.i_date = False
        self.i_module = False
        self.i_tool = False
        self.i_response = False
        for item in text:
            if type(text[item]) is dict:
                self.unravel_dict(text[item])
                if self.i_tool and self.i_module and self.i_date and self.i_email:
                    if not self.i_response:
                        self.append_item(itr)
                    return

    def run(self, pendo_key, outfile):
        url = "https://app.pendo.io/api/v1/report"
        resp = self.http.request('GET', url, headers = {
            'x-pendo-integration-key': pendo_key,
            'Content-Type': "application/json"
        })
        pendo_json = json.loads(resp.data)
        index=[]
        for i in range(1, len(pendo_json)+1):
            index.append(str(i))
        self.pendo_df = pd.DataFrame(columns=['email', 'date', 'module', 'tool'], index=index)
        itr = 1
        for pendo_dict in pendo_json:
            self.i_response = False
            self.parse_resp(pendo_dict, itr)
            itr += 1
        self.pendo_df.to_csv(outfile, sep=',', header=None)
